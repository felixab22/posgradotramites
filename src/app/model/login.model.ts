export class PersonaModel {
    public idpersona: number;
    public cargo?: string;
    public nombre: string;
    public apellido: string;
    public dni: number;
    public telefono: number;
    public correo: string;
    public estado: boolean;
    public idarea: number;
}
export class UserModel {
    public id?: number;
    public username: string;
    public password: string;
    public idpersona: number;
    public idrole: number;
}
export class RolModel {
    public idrol: number;
    public descripcionrol: string;
}

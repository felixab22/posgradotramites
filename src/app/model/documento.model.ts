export class TipoDocModel {
    public idtipodocumento?: number;
    public descripcion: string;
}
export class RemitenteModel {
    public idremitente?: number;
    public nombres?: string;
    public apellidos?: string;
    public dni?: string;

    public email?: string;
    public telefono: number;
    public direccion?: string;
    public tiporemitente: number;

    public razonsocial?: string;
    public ruc?: string;

}
export class AreaModel {
    public idarea: number;
    public descripcion: string;
}
export class TramiteModel {
    public iddocumento: number;
    public codigodoc?: string;
    public nrofolios: number;
    public fregistro?: Date;
    public descripcion: string;
    public tipoDocumento: number;
    public remitente: number;
    public asunto: number;
    public estado: number;
    // 1 = pendiente
    // 2 = atendido    
}
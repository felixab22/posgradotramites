export class AsuntoModel {
    public idasunto: number;
    public denominacion: string;
    public descripcion: string;
}
export class MovimientoModel {
    public idmovimiento: number;
    public asunto: string;
    public estadomovimiento: string;
    public docgenerado: string;
    public documento: number;
    public area: number;
    public fmovimiento?: Date;

}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { BuscaComponent } from './pages/busca/busca.component';
import { AuthGuard } from './core/auth.guard';
import { SituacionComponent } from './pages/situacion/situacion.component';
// import { SelectivePreloadingStrategyService } from './selective-preloading-strategy.service';


const routes: Routes = [
  { path: '', component: BuscaComponent },
  { path: 'situaciontramite', component: SituacionComponent },
  {
    path: 'Posgrado',
    component: PagesComponent,
    canActivate: [AuthGuard],
    children:
      [
        {
          path: 'Documento',
          loadChildren: './pages/documento/documento.module#DocumentoModule'
          // data: { preload: true }
        },
        // {
        //   path: 'Buscar',
        //   loadChildren: './pages/buscador/buscador.module#BuscadorModule'
        // },
        {
          path: 'Lista',
          loadChildren: './pages/lista/lista.module#ListaModule'
          // data: { preload: true }
        },
        {
          path: 'Mantenimiento',
          loadChildren: './pages/mantenimiento/mantenimiento.module#MantenimientoModule'
        }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
    // enableTracing: true, // <-- debugging purposes only
    // preloadingStrategy: SelectivePreloadingStrategyService
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from 'src/app/configuration/ip.configuration';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoDocModel, AreaModel } from '../../model/documento.model';
import { AsuntoModel } from 'src/app/model/mantenimiento.model';
import { PersonaModel, RolModel, UserModel } from 'src/app/model/login.model';
@Injectable({
    providedIn: 'root'
})
export class MantenimientoService {

    api = URL_SERVICIOS;
    constructor(private http: HttpClient) {

    }

    // si la lista esta vasia es 204
    // ======================= tipo documento ===================
    //   
    public getListaTipoDocumento(): Observable<TipoDocModel[]> {
        return this.http.get<TipoDocModel[]>(this.api + `/Documento/getAllTipoDocumento`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // guardar y editar un Seguimiento
    public saveOrUpdateTipoDocumento(documento: TipoDocModel): Observable<TipoDocModel> {
        return this.http.post<TipoDocModel>(this.api + `/Documento/saveOrUpdateTipoDocumento`, JSON.stringify(documento), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // ============ Area ==============
    public getListaArea(): Observable<AreaModel[]> {
        return this.http.get<AreaModel[]>(this.api + `/mantenimiento/getAllArea`);
    }

    // guardar y editar un Seguimiento
    public saveOrUpdateArea(documento: AreaModel): Observable<AreaModel> {
        return this.http.post<AreaModel>(this.api + `/mantenimiento/saveOrUpdateArea`, JSON.stringify(documento), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
    // ============ Asunto ==============
    public getListaAsunto(): Observable<AsuntoModel[]> {
        return this.http.get<AsuntoModel[]>(this.api + `/mantenimiento/getAllAsunto`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // guardar y editar un Seguimiento
    public saveOrUpdateAsunto(documento: AsuntoModel): Observable<AsuntoModel> {
        return this.http.post<AsuntoModel>(this.api + `/mantenimiento/saveOrUpdateAsunto`, JSON.stringify(documento), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // ============ Rol ==============
    public getListaRol(): Observable<RolModel[]> {
        return this.http.get<RolModel[]>(this.api + `/rol/getRol`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // guardar y editar un Seguimiento
    public saveOrUpdateRol(documento: RolModel): Observable<RolModel> {
        return this.http.post<RolModel>(this.api + `/rol/saveOrUpdateRol`, JSON.stringify(documento), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // ============ EMPLEADOS  ==============
    public getListaEmpleado(): Observable<PersonaModel[]> {
        return this.http.get<PersonaModel[]>(this.api + `/empleado/getAllEmpleado`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // guardar y editar un Seguimiento
    public saveOrUpdateEmpleado(documento: PersonaModel): Observable<PersonaModel> {
        return this.http.post<PersonaModel>(this.api + `/empleado/saveOrUpdateEmpleado`, JSON.stringify(documento), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }

    // ============ EMPLEADOS  ==============
    // public getListaUser(): Observable<PersonaModel[]> {
    //     return this.http.get<PersonaModel[]>(this.api + `/empleado/getAllEmpleado`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    // }

    // guardar y editar un Seguimiento
    public saveOrUpdateUser(documento: UserModel): Observable<UserModel> {
        return this.http.post<UserModel>(this.api + `/auth/signup`, documento, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
    }
}

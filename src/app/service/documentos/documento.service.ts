import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from 'src/app/configuration/ip.configuration';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoDocModel, RemitenteModel, TramiteModel } from '../../model/documento.model';
import { MovimientoModel } from '../../model/mantenimiento.model';
@Injectable({
  providedIn: 'root'
})
export class DocumentoService {

  api = URL_SERVICIOS;
  constructor(private http: HttpClient) {
  }
  // Listar Actas
  // si la lista esta vasia es 204


  // guardar y editar un Seguimiento
  public saveOrUpdateRemitente(remitente: RemitenteModel): Observable<RemitenteModel> {
    return this.http.post<RemitenteModel>(this.api + `/remitente/saveOrUpdateRemitente`, JSON.stringify(remitente), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }


  // obtener remitente por DNI
  public getOneRemitenteXdni(dni: string): Observable<RemitenteModel> {
    return this.http.get<RemitenteModel>(this.api + `/remitente/getRemitenteByDniOrRuc?data=${dni}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  // obtener remitente por DNI
  public getOneRemitentePogradoXdni(dni: string): Observable<RemitenteModel> {
    return this.http.get<RemitenteModel>(this.api + `/posgrado-unsch/getEstudianteByDni?dni=${dni}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  // =============== Documento ============= 

  public getListaDocPendientes(): Observable<TramiteModel[]> {
    return this.http.get<TramiteModel[]>(this.api + `/Documento/getAllDocumentosPendientes`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }
  // guardar y editar un Seguimiento
  public saveOrUpdateTramite(tramite: TramiteModel): Observable<TramiteModel> {
    return this.http.post<TramiteModel>(this.api + `/Documento/saveOrUpdateDocumento`, JSON.stringify(tramite), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  // =============== Documento ============= 

  public getListaAllMovimientoxDocumento(numDoc): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/movimiento/getAllMovimientoByDocumento?codigo=${numDoc}`);
  }
  // guardar y editar un Seguimiento
  public saveOrUpdateMovimiento(movimiento: MovimientoModel): Observable<MovimientoModel> {
    return this.http.post<MovimientoModel>(this.api + `/movimiento/saveOrUpdateMovimiento`, JSON.stringify(movimiento), { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

  // ============ PENDIENTES ================

  public getListaAllMovimientosXArea(numDoc): Observable<MovimientoModel[]> {
    return this.http.get<MovimientoModel[]>(this.api + `/movimiento/getAllMovimientoDocByArea?idarea=${numDoc}`, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('usertoken') }) });
  }

}


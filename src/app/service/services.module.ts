import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    DocumentoService,
    MantenimientoService
} from './services.index';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    providers: [
        DocumentoService,
        MantenimientoService
    ],
    declarations: []
})
export class ServiceModule { }

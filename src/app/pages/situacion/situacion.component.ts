import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-situacion',
  templateUrl: './situacion.component.html',
  styleUrls: ['./situacion.component.scss']
})
export class SituacionComponent implements OnInit {
  alternate: boolean = true;
  toggle: boolean = true;
  color: boolean = false;
  size: number = 40;
  expandEnabled: boolean = true;
  side = 'left';
  seguido: any;
  documento: any;
  constructor(
    private _router : Router
  ) { 
    if(localStorage.getItem('documento') === null) {
      this._router.navigate(['']);
    }
  }

  ngOnInit() {
    this.documento = JSON.parse(localStorage.getItem('documento'));
    this.seguido = JSON.parse(localStorage.getItem('movimientos'));
    // if(this.documento === null) {
    //   this._router.navigate(['']);
    // }
    // console.log(this.documento);
    // console.log(this.seguido);
    
  }


  onHeaderClick(event) {
    if (!this.expandEnabled) {
      event.stopPropagation();
    }
  }

  onDotClick(event) {
    if (!this.expandEnabled) {
      event.stopPropagation();
    }
  }

  onExpandEntry(expanded, index) {
    console.log(`Expand status of entry #${index} changed to ${expanded}`)
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    console.log('destruido');
    localStorage.clear();
  }
}

import { Component, OnInit } from '@angular/core';
import { RemitenteModel, TipoDocModel, TramiteModel } from '../../../model/documento.model';
import { DocumentoService, MantenimientoService } from 'src/app/service/services.index';
import { AsuntoModel } from 'src/app/model/mantenimiento.model';
declare var swal: any;
@Component({
  selector: 'app-nuevo-documento',
  templateUrl: './nuevo-documento.component.html',
  styleUrls: ['./nuevo-documento.component.scss']
})
export class NuevoDocumentoComponent implements OnInit {
  procedencias: any
  newRemitente: RemitenteModel;
  Remitentes: RemitenteModel[];
  RemitDNI: RemitenteModel;
  buscarxDNI = '';
  documentos: TipoDocModel[];
  newTramite: TramiteModel;
  nombreRemitente = '';
  asuntoseleccionado = '';
  Asuntos: AsuntoModel[];
  constructor(
    private _documentoSrv: DocumentoService,
  ) {
    this.newTramite = new TramiteModel();
    this.newRemitente = new RemitenteModel();
    this.RemitDNI = new RemitenteModel();
    this.Remitentes = new Array<RemitenteModel>();
  }

  ngOnInit() {
    this.listartipoDocumento();
    this.listarAsuntos();
    this.newRemitente.tiporemitente = 1;
  }

  listartipoDocumento() {
    this.documentos = JSON.parse(localStorage.getItem('listaDocumentos'));
  }
  listarAsuntos() {
    this.Asuntos = JSON.parse(localStorage.getItem('listaAsuntos'));
  }

  selecTipoDocumento(documento: any): void {
    // console.log(documento);
    this.newTramite.tipoDocumento = parseInt(documento);
  }

  selection(tipo: any) {
    this.newRemitente.tiporemitente = parseInt(tipo);
  }
  crearNuevoRemitente(mdbmodal2) {
    this._documentoSrv.saveOrUpdateRemitente(this.newRemitente).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {
        swal("Bien!", "Remitente creado", "success");
        this.newRemitente = new RemitenteModel();
        mdbmodal2.hide();
      } else {
        swal("Mal!", "Remitente no creado!", "error");
      }

    });
  }
  buscarXdni(dni: string) {
    console.log(dni);

    this._documentoSrv.getOneRemitenteXdni(dni).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {
        this.RemitDNI = res.data;
        this.Remitentes.push(this.RemitDNI)
      } else {
        if (res.code === 204) {
          this._documentoSrv.getOneRemitentePogradoXdni(dni).subscribe((res: any) => {
            console.log(res);
            if (res.code === 200) {
              this.newRemitente.apellidos = res.data.apellidos;
              this.newRemitente.dni = res.data.dni;
              this.newRemitente.nombres = res.data.nombres;
              this.newRemitente.telefono = res.data.telefono;
              this._documentoSrv.saveOrUpdateRemitente(this.newRemitente).subscribe((res: any) => {
                console.log(res);
                if (res.code === 200) {
                  this.newRemitente = new RemitenteModel();
                  this.Remitentes.push(res.data);
                } else {
                  console.log("error 10");

                }

              });
            }
          });
        }
      }



    });

  }
  remitenteSeleccionado(remitente: RemitenteModel, tipo, model) {
    this.newTramite.remitente = remitente.idremitente;
    console.log(remitente, tipo);
    if (tipo === 'natural') {
      this.nombreRemitente = remitente.apellidos + ', ' + remitente.nombres
    } else {
      this.nombreRemitente = remitente.razonsocial;
    }
    model.hide()
  }
  AsuntoSeleccionado(asunto: AsuntoModel, asuntoModel) {
    this.newTramite.asunto = asunto.idasunto;
    this.asuntoseleccionado = asunto.denominacion;
    asuntoModel.hide()
  }
  GuardarYactualizarTramite() {
    this.newTramite.estado = 1;
    this._documentoSrv.saveOrUpdateTramite(this.newTramite).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {
        swal({
          title: "Registro guardado correctamente",
          text: `Codigo de tramite: ${res.data.codigodoc}`,
          icon: "success",
          button: "Aceptar",
        });

        swal({
          title: "Bien",
          text: "Registro guardado correctamente",
          icon: "success",
          buttons: true,
          dangerMode: true,
        })
          .then((willDelete) => {
            if (willDelete) {
              swal({
                title: `Codigo de tramite: ${res.data.codigodoc}`,
                icon: "success",
                button: "Aceptar",
              });
              this.newTramite = new TramiteModel();
              this.nombreRemitente = '';
              this.asuntoseleccionado = '';
            } else {
              swal("Your imaginary file is safe!");
            }
          });
      }
    });
  }
}

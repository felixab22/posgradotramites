import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NuevoDocumentoComponent } from './nuevo-documento.component';


const routes: Routes = [
  {
    path: '',
    component: NuevoDocumentoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuevoDocumentoRoutingModule { }

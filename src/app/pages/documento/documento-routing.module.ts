import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NuevoDocumentoComponent } from './nuevo-documento/nuevo-documento.component';


const routes: Routes = [
  {
    path: 'Nuevo',
    component: NuevoDocumentoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentoRoutingModule { }

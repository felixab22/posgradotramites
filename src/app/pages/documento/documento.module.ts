import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { RouterModule } from '@angular/router';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { DocumentoRoutingModule } from './documento-routing.module';
import { NuevoDocumentoComponent } from './nuevo-documento/nuevo-documento.component';
import { ServiceModule } from 'src/app/service/services.module';


@NgModule({
  declarations: [NuevoDocumentoComponent],
  imports: [
    CommonModule,
    //desde aqui para MDBootstrap
    // ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'Your_api_key'
    }),
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    DocumentoRoutingModule,
    ServiceModule
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DocumentoModule { }

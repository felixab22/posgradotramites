import { Component, OnInit } from '@angular/core';
import { AreaModel } from 'src/app/model/documento.model';
import { MantenimientoService } from 'src/app/service/services.index';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {

  areas: AreaModel[];
  newArea: AreaModel;
  constructor(
    private _mantenimientoSrv: MantenimientoService
  ) {
    this.newArea = new AreaModel();
    this.areas = new Array<AreaModel>();
  }

  ngOnInit() {
    this.listarAreasAll();
  }

  listarAreasAll() {
    this.areas = JSON.parse(localStorage.getItem('listaAreas'));
  }

  GuardarAndActualizaArea(basicmodel) {
    this._mantenimientoSrv.saveOrUpdateArea(this.newArea).subscribe(res => {
      console.log(res);
      this.areas.push(this.newArea);
      localStorage.setItem('listaAreas', JSON.stringify(this.areas));
      this.newArea = new AreaModel();
    });
    basicmodel.hide()
  }

  NuevoArea(basicmodel) {
  }
}

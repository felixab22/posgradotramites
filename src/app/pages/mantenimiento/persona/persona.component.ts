import { Component, OnInit } from '@angular/core';
import { PersonaModel, RolModel, UserModel } from '../../../model/login.model';
import { DocumentoService, MantenimientoService } from 'src/app/service/services.index';
import { AreaModel } from 'src/app/model/documento.model';
import { FormControl, Validators, FormGroup } from '@angular/forms';

declare var swal: any;
@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss']
})
export class PersonaComponent implements OnInit {

  registergForm: FormGroup;
  newUser: UserModel;
  newPersona: PersonaModel;
  empleados: PersonaModel[];
  areas: AreaModel[];
  estados: { id: boolean; descripcion: string; }[];
  roles: RolModel[];
  paswoRepetido = '';
  constructor(
    private _documentoSrv: DocumentoService,
    private _mantenimientoSrv: MantenimientoService
  ) {

    this.newPersona = new PersonaModel();
    this.newUser = new UserModel();
    this.roles = new Array<RolModel>();
    this.empleados = new Array<PersonaModel>();
    this.estados = [
      { id: true, descripcion: 'ACTIVO' },
      { id: false, descripcion: 'DESACTIVO' }
    ]
  }

  ngOnInit() {
    this.ListarAllRoles();
    this.listarPersonal();
    this.listarAreasAll();
    this.registergForm = new FormGroup({
      modalFormRegisterEmail: new FormControl('', [Validators.required]),
      modalFormRegisterPassword: new FormControl('', Validators.required),
      modalFormRegisterRepeatPassword: new FormControl('', Validators.required)
    });
  }
  // NuevoUser() {
  //   console.log(this.registergForm.value);
  //   this._mantenimientoSrv.saveOrUpdateUser(this.newUser).subscribe((res: any) => {
  //     console.log(res);
  //   });
  //   console.log(this.newUser);
  // }
  onSubmitRegidter() {
    if (this.registergForm.valid) {
      console.log(this.registergForm.value);
      this.newUser.username = this.registergForm.value.modalFormRegisterEmail;
      this.newUser.password = this.registergForm.value.modalFormRegisterPassword;
      console.log(this.newUser);
      this._mantenimientoSrv.saveOrUpdateUser(this.newUser).subscribe((res: any) => {
        console.log(res);
      });

      this.onReserForm();
    }
    else {
      console.error('no valido');

    }
  }
  selecTipoRol(rol) {
    console.log(rol);
    this.newUser.idrole = parseInt(rol);
  }
  listarAreasAll() {
    this.areas = JSON.parse(localStorage.getItem('listaAreas'));
  }

  ListarAllRoles() {
    this.roles = JSON.parse(localStorage.getItem('listarRoles'));
  }

  onReserForm() {
    this.registergForm.reset();
  }

  get modalFormRegisterEmail() {
    return this.registergForm.get('modalFormRegisterEmail');
  }

  get modalFormRegisterPassword() {
    return this.registergForm.get('modalFormRegisterPassword');
  }

  get modalFormRegisterRepeatPassword() {
    return this.registergForm.get('modalFormRegisterRepeatPassword');
  }

  selecArea(area) {
    console.log(area);
    this.newPersona.idarea = parseInt(area);
  }
  selecEstado(estado: boolean) {
    console.log(estado);
    this.newPersona.estado = estado;
  }
  listarPersonal() {
    this._mantenimientoSrv.getListaEmpleado().subscribe((res: any) => {
      if (res.code === 200) {
        this.empleados = res.data;
      } else {
        console.log('lista vasia');
      }
    });
  }

  saveOrUpdateEmpleado(model) {
    this._mantenimientoSrv.saveOrUpdateEmpleado(this.newPersona).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {
        swal('Bien!', 'Guardado!', 'success');
        // this.empleados.push(this.newPersona);
        this.listarPersonal();
        this.newPersona = new PersonaModel();
        model.hide();
      } else {
        swal('Mal!', 'Los campos con * son obligatorios', 'warning');
      }
    });

  }
  saveOrUpdateUsuario(model) {
    model.hide();
  }

  registerUser(persona, model) {
    model.show();
    this.newUser.idpersona = persona.idpersona;

  }
}

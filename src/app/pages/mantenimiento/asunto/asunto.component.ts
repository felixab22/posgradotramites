import { Component, OnInit } from '@angular/core';
import { AsuntoModel } from '../../../model/mantenimiento.model';
import { MantenimientoService } from 'src/app/service/services.index';

@Component({
  selector: 'app-asunto',
  templateUrl: './asunto.component.html',
  styleUrls: ['./asunto.component.scss']
})
export class AsuntoComponent implements OnInit {

  newAsunto: AsuntoModel;
  Asuntos: AsuntoModel[];
  constructor(
    private _mantenimientoSrv: MantenimientoService
  ) {

    this.newAsunto = new AsuntoModel();
    this.Asuntos = new Array<AsuntoModel>();
  }

  ngOnInit() {
    this.listarAsuntos();
  }

  listarAsuntos() {
    this.Asuntos = JSON.parse(localStorage.getItem('listaAsuntos'));
  }

  GuardarAndActualizaAsunto(bascimodel) {
    this._mantenimientoSrv.saveOrUpdateAsunto(this.newAsunto).subscribe(res => {
      console.log(res);
      this.Asuntos.push(this.newAsunto);
      localStorage.setItem('listaAsuntos', JSON.stringify(this.Asuntos));
      this.newAsunto = new AsuntoModel();
    });
    bascimodel.hide();
  }

}

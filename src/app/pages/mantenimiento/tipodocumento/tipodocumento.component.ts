import { Component, OnInit } from '@angular/core';
import { MantenimientoService } from 'src/app/service/services.index';
import { TipoDocModel } from '../../../model/documento.model';
declare var swal: any;
@Component({
  selector: 'app-tipodocumento',
  templateUrl: './tipodocumento.component.html',
  styleUrls: ['./tipodocumento.component.scss']
})
export class TipodocumentoComponent implements OnInit {
  documentos: TipoDocModel[];
  newDocument: TipoDocModel;
  constructor(
    private _mantenimientoSrv: MantenimientoService,
  ) {
    this.newDocument = new TipoDocModel();
  }

  ngOnInit() {
    this.listartipoDocumento();
  }
  NuevoDocumento(model) {
    this._mantenimientoSrv.saveOrUpdateTipoDocumento(this.newDocument).subscribe((res: any) => {
      if (res.code === 200) {
        swal('Bien!', 'Guardado!', 'success');
        this.documentos.push(this.newDocument);
        localStorage.setItem('listaDocumentos', JSON.stringify(this.documentos));
        this.newDocument = new TipoDocModel();
      } else {
        if (res.code === 204) {
          swal('Mal!', 'Lista vacía', 'warning');
        }
      }
    });
    model.hide();
  }
  listartipoDocumento() {
    this.documentos = JSON.parse(localStorage.getItem('listaDocumentos'));
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipodocumentoComponent } from './tipodocumento/tipodocumento.component';
import { AsuntoComponent } from './asunto/asunto.component';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';
import { UsuarioComponent } from './usuario/usuario.component';


const routes: Routes = [
  {
    path: 'Bienvenido',
    component: BienvenidoComponent
  },
  {
    path: 'Tipo-documento',
    component: TipodocumentoComponent
  },
  {
    path: 'Usuarios',
    component: UsuarioComponent
  }
  ,
  {
    path: 'Asuntos',
    component: AsuntoComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenimientoRoutingModule { }

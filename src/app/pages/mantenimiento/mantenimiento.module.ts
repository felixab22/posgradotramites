import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MantenimientoRoutingModule } from './mantenimiento-routing.module';
import { TipodocumentoComponent } from './tipodocumento/tipodocumento.component';
import { AreaComponent } from './area/area.component';
import { AsuntoComponent } from './asunto/asunto.component';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { RolComponent } from './rol/rol.component';
import { PersonaComponent } from './persona/persona.component';


@NgModule({
  declarations: [TipodocumentoComponent, AreaComponent, AsuntoComponent, BienvenidoComponent, UsuarioComponent, RolComponent, PersonaComponent],
  imports: [
    CommonModule,
    //desde aqui para MDBootstrap
    // ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'Your_api_key'
    }),
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    //siempre importar,    
    MantenimientoRoutingModule
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MantenimientoModule { }

import { Component, OnInit } from '@angular/core';
import { RolModel } from 'src/app/model/login.model';
import { MantenimientoService } from 'src/app/service/services.index';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.scss']
})
export class RolComponent implements OnInit {


  roles: RolModel[];
  newRol: RolModel;
  constructor(
    private _mantenimientoSrv: MantenimientoService
  ) {
    this.newRol = new RolModel();
    this.roles = new Array<RolModel>();
  }

  ngOnInit() {
    this.ListarAllRoles();
  }

  ListarAllRoles() {
    this.roles = JSON.parse(localStorage.getItem('listarRoles'));
  }

  GuardarAndActualizaRol(basicmodel) {
    this._mantenimientoSrv.saveOrUpdateRol(this.newRol).subscribe(res => {
      console.log(res);
      this.roles.push(this.newRol);
      localStorage.setItem('listarRoles', JSON.stringify(this.roles));
      this.newRol = new RolModel();
    });
    basicmodel.hide()
  }

  NuevoArea(basicmodel) {
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { MantenimientoService } from 'src/app/service/services.index';
import { AreaModel, TipoDocModel } from 'src/app/model/documento.model';
import { AsuntoModel } from 'src/app/model/mantenimiento.model';
import { PersonaModel } from '../../../model/login.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bienvenido',
  templateUrl: './bienvenido.component.html',
  styleUrls: ['./bienvenido.component.scss']
})
export class BienvenidoComponent implements OnInit {
  pendientes: { id: number; tipoDoc: string; numDoc: string; remitente: string; folio: number; fecha: string; }[];
  @ViewChild('frame', { static: true }) public contentModal;
  public name: string;
  areas: AreaModel[];
  documentos: TipoDocModel[];
  asuntos: AsuntoModel[];
  ingresante = '';
  link: string;
  constructor(
    private _mantenimientoSrv: MantenimientoService,
    private _route: Router
  ) {
    this.areas = new Array<AreaModel>();
  }

  ngOnInit() {
    this.ingresante = localStorage.getItem('autorization');
    console.log(this.ingresante);
    this.listarAreasAll();
    this.listarRoles();
    setTimeout(() => {
      this.contentModal.show();
    }, 1000);
    if (this.ingresante === 'SECRE' || this.ingresante === 'ADMIN') {
      this.link = '/Posgrado/Documento/Nuevo';
      this.listartipoDocumento();
      this.listartipoAsunto();
    }
    if (this.ingresante === 'OTROS') {
      this.link = '/Posgrado/Lista/Proceso';
    }

  }

  ingresar() {
    this._route.navigate([this.link]);
    // [routerLink]="['/Posgrado/Documento/Nuevo']"
  }

  listarAreasAll() {
    this._mantenimientoSrv.getListaArea().subscribe((res: any) => {
      if (res.code === 200) {
        this.areas = res.data;
        localStorage.setItem('listaAreas', JSON.stringify(this.areas));
      } else {
        console.log('listaAreas vacio');
      }
    });
  }
  listarRoles() {
    this._mantenimientoSrv.getListaRol().subscribe((res: any) => {
      if (res.code === 200) {
        this.documentos = res.data;
        localStorage.setItem('listarRoles', JSON.stringify(this.documentos));
      } else {
        console.log('listarRoles vacio');
        console.log(res);
      }
    });
  }
  listartipoDocumento() {
    this._mantenimientoSrv.getListaTipoDocumento().subscribe((res: any) => {
      if (res.code === 200) {
        this.documentos = res.data;
        localStorage.setItem('listaDocumentos', JSON.stringify(this.documentos));
      } else {
        console.log('listaDocumentos vacio');
      }
    });
  }

  listartipoAsunto() {
    this._mantenimientoSrv.getListaAsunto().subscribe((res: any) => {
      if (res.code === 200) {
        this.asuntos = res.data;
        localStorage.setItem('listaAsuntos', JSON.stringify(this.asuntos));
      } else {
        console.log('listaAsuntos vacio');
      }
    });
  }

  iniciar(basicModal) {
    basicModal.show();
  }
}

import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { ModalModule, TooltipModule, PopoverModule, ButtonsModule } from 'ng-uikit-pro-standard'
import { BuscaComponent } from './busca/busca.component';
import { SituacionComponent } from './situacion/situacion.component';
import { MglTimelineModule } from 'angular-mgl-timeline';
@NgModule({
  declarations: [
    PagesComponent,
    BuscaComponent,
    SituacionComponent
  ],
  imports: [
    CommonModule,
    MglTimelineModule,
    //desde aqui para MDBootstrap
    // ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'Your_api_key'
    }),
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    //siempre importar,
    SharedModule,
    ModalModule, TooltipModule, PopoverModule, ButtonsModule
  ],
  exports: [BuscaComponent, SituacionComponent],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PagesModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendientesComponent } from './pendientes/pendientes.component';
import { ProcesoComponent } from './proceso/proceso.component';


const routes: Routes = [
  {
    path: 'Pendientes',
    component: PendientesComponent
  },
  {
    path: 'Proceso',
    component: ProcesoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListaRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { MovimientoModel } from 'src/app/model/mantenimiento.model';
import { AreaModel } from 'src/app/model/documento.model';
import { DocumentoService } from 'src/app/service/services.index';

@Component({
  selector: 'app-pendientes',
  templateUrl: './pendientes.component.html',
  styleUrls: ['./pendientes.component.scss']
})
export class PendientesComponent implements OnInit {
  pendientes: any;
  boton = 1;
  areas: AreaModel[];
  persona: any;
  constructor(
    private _documentoSrv: DocumentoService
  ) {
  }

  ngOnInit() {
    this.persona = JSON.parse(localStorage.getItem('personal'));
    this.selecArea();
  }
  selecArea() {

    this._documentoSrv.getListaAllMovimientosXArea(this.persona.idarea.idarea).subscribe((res: any) => {
      console.log(res.data);
      this.pendientes = res.data;
    });
  }
  // listarAreasAll() {
  //   this.areas = JSON.parse(localStorage.getItem('listaAreas'));
  // }

}

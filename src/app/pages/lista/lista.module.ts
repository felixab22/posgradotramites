import { CommonModule } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ListaRoutingModule } from './lista-routing.module';
import { PendientesComponent } from './pendientes/pendientes.component';
import { ProcesoComponent } from './proceso/proceso.component';


@NgModule({
  declarations: [PendientesComponent, ProcesoComponent],
  imports: [
    CommonModule,
    //desde aqui para MDBootstrap
    // ToastModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'Your_api_key'
    }),
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ListaRoutingModule
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ListaModule { }

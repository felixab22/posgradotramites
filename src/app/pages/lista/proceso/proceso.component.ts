import { Component, OnInit } from '@angular/core';
import { DocumentoService } from 'src/app/service/services.index';
import { AreaModel, TramiteModel } from 'src/app/model/documento.model';
import { MovimientoModel } from 'src/app/model/mantenimiento.model';
declare var swal: any;
@Component({
  selector: 'app-proceso',
  templateUrl: './proceso.component.html',
  styleUrls: ['./proceso.component.scss']
})
export class ProcesoComponent implements OnInit {
  proceso: any;
  boton = 2;
  areas: AreaModel[];
  newMovimiento: MovimientoModel;
  newTramite: TramiteModel;

  constructor(
    private _documentoSrv: DocumentoService,

  ) {
    this.newMovimiento = new MovimientoModel();
    this.newTramite = new TramiteModel();
  }

  ngOnInit() {
    this.ListarPendientes();
    this.listarAreasAll();
  }
  ListarPendientes() {
    this._documentoSrv.getListaDocPendientes().subscribe((res: any) => {
      console.log(res);
      this.proceso = res.data;
    });
  }

  NuevoMovimieno(basicModal1) {
    console.log(this.newMovimiento);
    this._documentoSrv.saveOrUpdateMovimiento(this.newMovimiento).subscribe((res: any) => {
      if (res.code === 200) {
        console.log(res);
        this.GuardarYactualizarTramite();
        this.newMovimiento = new MovimientoModel();
        swal({
          title: "Good job!",
          text: "You clicked the button!",
          icon: "success",
          button: "Aww yiss!",
        });
      }
    });
    basicModal1.hide();

  }
  iniciarMovimiento(tramite, model) {
    model.show();
    this.newMovimiento.documento = tramite.iddocumento;
    this.newMovimiento.estadomovimiento = 'DERIVADO';

    this.newTramite.asunto = tramite.asunto.idasunto;
    this.newTramite.codigodoc = tramite.codigodoc;
    this.newTramite.descripcion = tramite.descripcion;
    this.newTramite.fregistro = tramite.fregistro;
    this.newTramite.iddocumento = tramite.iddocumento;
    this.newTramite.nrofolios = tramite.nrofolios;
    this.newTramite.remitente = tramite.remitente.idremitente;
    this.newTramite.tipoDocumento = tramite.tipoDocumento.idtipodocumento
    console.log(tramite);
  }
  GuardarYactualizarTramite() {
    console.log(this.newTramite);
    this.newTramite.estado = 2;
    this._documentoSrv.saveOrUpdateTramite(this.newTramite).subscribe((res: any) => {
      if (res.code === 200) {
        this.deleteLista(res.data.iddocumento);
        this.newMovimiento = new MovimientoModel();
        console.log('cambio estado a 2');
      }
    });
  }
  public deleteLista(iddocumento: number): void {
    this.proceso = this.proceso.filter(u => iddocumento !== u.iddocumento);
  }

  listarAreasAll() {
    this.areas = JSON.parse(localStorage.getItem('listaAreas'));
  }
  selecArea(area) {
    this.newMovimiento.area = parseInt(area);
  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';
import { DocumentoService } from 'src/app/service/services.index';
declare var swal: any;

@Component({
  selector: 'app-busca',
  templateUrl: './busca.component.html',
  styleUrls: ['./busca.component.scss']
})
export class BuscaComponent implements OnInit {
  date = new Date();
  private anioactual: string;
  buscador = '';
  validatingForm: FormGroup;
  registergForm: FormGroup;
  emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(
    private _router: Router,
    private authService: AuthService,
    private _documentoSrv: DocumentoService,
  ) {
    this.anioactual = String(this.date).substr(8, 2);
  }

  ngOnInit() {
    this.validatingForm = new FormGroup({
      modalFormLoginEmail: new FormControl('', [Validators.required,]),
      modalFormLoginPassword: new FormControl('', Validators.required)
    });
    this.registergForm = new FormGroup({
      modalFormRegisterEmail: new FormControl('', [Validators.required]),
      modalFormRegisterPassword: new FormControl('', Validators.required),
      modalFormRegisterRepeatPassword: new FormControl('', Validators.required)
    });
  }
  onSubmit() {
    if (this.validatingForm.valid) {
      // console.log(this.validatingForm.value.modalFormLoginEmail);
      // console.log(this.validatingForm.value.modalFormLoginPassword);
      this.authService.attemptAuth(this.validatingForm.value.modalFormLoginEmail, this.validatingForm.value.modalFormLoginPassword)
        .subscribe(
          data => {
            localStorage.setItem('hoy', this.anioactual);
            localStorage.setItem('personal', JSON.stringify(data.persona));
            localStorage.setItem('usertoken', data.accessToken);
            localStorage.setItem('autorization', data.authorities[0].authority);
            this._router.navigate(['Posgrado/Mantenimiento/Bienvenido']);
          },
          error => {
            swal("Mal!", "Credenciale incorrectos!", "warning")
          }
        );



      this.onReserForm();

      this._router.navigate(['Posgrado/Mantenimiento/Bienvenido']);
    }
    else {
      console.error('no valido');

    }
    // [routerLink]="['/Posgrado/Mantenimiento/Bienvenido']"

  }

  onSubmitRegidter() {
    if (this.registergForm.valid) {
      console.log(this.registergForm.value);
      this.onReserForm();
    }
    else {
      console.error('no valido');

    }
  }

  onReserForm() {
    this.validatingForm.reset();
    this.registergForm.reset();
  }
  get modalFormLoginEmail() {
    return this.validatingForm.get('modalFormLoginEmail');
  }

  get modalFormLoginPassword() {
    return this.validatingForm.get('modalFormLoginPassword');
  }

  get modalFormRegisterEmail() {
    return this.registergForm.get('modalFormRegisterEmail');
  }

  get modalFormRegisterPassword() {
    return this.registergForm.get('modalFormRegisterPassword');
  }

  get modalFormRegisterRepeatPassword() {
    return this.registergForm.get('modalFormRegisterRepeatPassword');
  }

  movimientoBuscarPorCodigo() {
    if (this.buscador.length !== 0)
      this._documentoSrv.getListaAllMovimientoxDocumento(this.buscador).subscribe((res: any) => {
        console.log(res);
        if(res.code === 204) {
           swal('MAL!', 'EL CÓDIGO NO EXISTE', 'warning');
        } else {
          localStorage.setItem('documento', JSON.stringify(res.dataDocumento));
          localStorage.setItem('movimientos', JSON.stringify(res.dataMovimientos));
          this._router.navigate(['/situaciontramite']);
          // [routerLink]="['/situaciontramite']"

        }
      });
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  urlimg = '../../../assets/img/avatars/Velazquez.jpg'
  nombre: string;
  personal: any;
  date = new Date();
  anioactual: number;
  ingresante = '';
  verInicio = true;
  verMantenimiento = true;
  constructor(
    private _router: Router,


  ) { }

  ngOnInit() {

    this.ingresante = localStorage.getItem('autorization');
    if (this.ingresante === 'SECRE') {
      this.verInicio = true;
      this.verMantenimiento = false;
    }
    if (this.ingresante === 'OTROS') {
      this.verInicio = false;
      this.verMantenimiento = false;
    }
    // this.imprimirFiles();
    if (localStorage.getItem('personal')) {
      this.personal = JSON.parse(localStorage.getItem('personal'));
      this.nombre = this.personal.nombre + ' ' + this.personal.apellido || 'Usuario';
    }
    const anioactual = String(this.date).substr(8, 2);
    this.anioactual = parseInt(anioactual);
    const ayer = parseInt(localStorage.getItem('hoy'));
    if (ayer !== this.anioactual) {
      this.cerrarseccion();

    }

  }
  cerrarseccion() {
    console.log('hola');
    localStorage.removeItem('usertoken');
    localStorage.removeItem('personal');
    localStorage.removeItem('hoy');
    localStorage.removeItem('autorization');
    this._router.navigate(['']);
  }
  // imprimirFiles() {    
  //   this.urlimg = '../../../assets/img/avatars/Velazquez.jpg'
  //   // this._uploadFilesSrv.getListaFilesxAreayPaciente('perfil', user.id_personal).subscribe(res => {
  //   //   if (res.length === 0) {
  //   //     console.log('no existe foto');
  //   //   } else {
  //   //     const imagenperfil = res[res.length - 1];
  //   //     this.urlimg = `${URL_SERVICIOS}/file/${imagenperfil}?nombrearea=perfil&codigo=${user.id_personal}`;
  //   //   }
  //   // });


  // }

}
